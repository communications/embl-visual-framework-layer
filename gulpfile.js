var gulp = require('gulp');
var $    = require('gulp-load-plugins')();
var gulpSequence = require('gulp-sequence').use(gulp);
var browserSync = require('browser-sync').create();

var sassPaths = [
  'bower_components/normalize.scss/sass',
  'bower_components/foundation-sites/scss',
  'bower_components/motion-ui/src'
];


gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      // proxy: "https://work.allaboutken.com",
      baseDir: './public'
    }
  },function(){
    // something you want to do
  });
});

gulp.task('sass', function() {
  return gulp.src('scss/theme-embl.scss')
    .pipe($.sass({
      includePaths: sassPaths
      // outputStyle: 'compressed' // if css compressed **file size**
    })
      .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(gulp.dest('public/css'));
});

gulp.task('refreshBrowser', function () {
  setTimeout(function() {
    console.log('Reloading browser windows.');
    browserSync.reload();
  }, 500); // wait for the filesystem to write
});


gulp.task('dev', ['sass', 'browser-sync'], function() {
  var watcher = gulp.watch(['./scss/**/*.scss','./public/index.html'], function(event) {
    console.log('File  ' + event.type + ': ' + event.path);
    console.log('Running tasks...');
    gulpSequence('sass', 'refreshBrowser')(function (err) {
      if (err) console.log(err)
    })
  });
  // gulp.watch(['scss/**/*.scss'], ['sass']);
});

gulp.task('default', ['sass']);
