# EMBL Visual Framework layer

This is a short-term tactical solution to implement the beta EMBL look and feel atop the the EBI Visual Framework. This will be superseded by the EMBL Design Dialect + Design Language for Life Sciences in late 2018/early 2019.

This is under active development, but here are useful links

- Demo page: https://master-branch-embl-visual-framework-layer-grp-stratcom.surge.sh
- CSS: https://master-branch-embl-visual-framework-layer-grp-stratcom.surge.sh/css/theme-embl.css


---
# No non-developers past this point
---

## Under the hood

This is a stripped down Foundation for Sites installation through npm.

To develop, you'll need to:

1. Clone this repo
1. Open terminal and type `npm install`
1. To build `gulp`
1. To develop locally `gulp dev`
